import { Navigation, AppTitle } from '../../layout'

const Navbar = ({ title, bgColor }) =>    
  <Navigation bgColor={bgColor}>
    <img src="chatterbox.png"  width="30" height="30" alt="logo" />
    <AppTitle>{title}</AppTitle>
  </Navigation>

export default Navbar
