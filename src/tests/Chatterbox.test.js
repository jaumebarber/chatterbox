import { fireEvent, render, screen, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import App from '../components/Chatterbox'

describe('Chatterbot', () => {
  const renderChatterbox = () => render(<App />)
  const queryByText = text => screen.queryByText(text)
  const queryByLabelText = label => screen.queryByLabelText(label)
  const fetchChatBox = () => queryByLabelText('Type a message')
  const fetchSendButton = () => queryByLabelText('Send a message')
  const fetchMessage = text => queryByLabelText(text)
  const sendBtn = () => fetchSendButton()
  const click = button => fireEvent.click(button)
  const type = (input, message) => userEvent.type(input, message)

  test('salutes you ;)', async () => {
    // Render the App
    renderChatterbox()
    const appName = queryByText(/Chatterbox/i)
    // Hope to find the App title
    expect(appName).toBeInTheDocument()
    await waitFor(() => {
    // Expect to receive a message from Chatterbot after 3 secs
      const chatterbotSalute = queryByText(/Hello human!/i)
      expect(chatterbotSalute).toHaveTextContent('Hello human!')}, { timeout : 4000})
  })

  test('and you can send messages to Chatterbot', () => {
    renderChatterbox()
    // Type a message
    const chatBox = fetchChatBox()
    type(chatBox, 'Hi Chatterbot!')
    // Send it
    const sendBtn = fetchSendButton()
    click(sendBtn)
    // Expect it to be there and match
    const message = queryByText(/Hi Chatterbot!/i)
    expect(message).toBeInTheDocument()
    expect(message).toHaveTextContent('Hi Chatterbot!')
  })

  test('but you can retire them after sending', () => {
    renderChatterbox()
    // Type a message
    const chatBox = fetchChatBox()
    type(chatBox, 'Stupid')
    // Send it
    const sendBtn = () => fetchSendButton()
    click(sendBtn())
    // Now type another message and send it
    type(chatBox, 'Idiot!')
    click(sendBtn())
    // Expect both messages to be there
    const message = queryByText('Stupid')
    const anotherMessage = queryByText('Idiot!')
    expect(message).toBeInTheDocument()
    expect(anotherMessage).toBeInTheDocument()
    // Delete it
    const deleteBtn = queryByLabelText('Delete Stupid')
    click(deleteBtn)
    // Expect only the first one to be gone
    expect(message).not.toBeInTheDocument()
    expect(anotherMessage).toBeInTheDocument()
  })

  test('all of them at once or some at a time, at will', () => {
    renderChatterbox()
    const chatBox = fetchChatBox()
    // Type and send 3 different messages
    type(chatBox, 'I cheated on you with')
    click(sendBtn())
    type(chatBox, 'Well... it\'s hard to say')
    click(sendBtn())
    type(chatBox, 'Telegram')
    click(sendBtn())
    // Fetch them
    const message = fetchMessage('I cheated on you with')
    const anotherMessage = fetchMessage('Well... it\'s hard to say')
    const yetAnotherOne = fetchMessage('Telegram')
    // Select two of them
    click(message)
    click(anotherMessage)
    // Click Delete all selected button
    const deleteAllBtn = queryByLabelText('Delete all selected')
    click(deleteAllBtn)
    // Expect both selected messages to be gone
    expect(message).not.toBeInTheDocument()
    expect(anotherMessage).not.toBeInTheDocument()
    expect(yetAnotherOne).toBeInTheDocument()
  })

  test('but silence can\'t be sent', () => {
    renderChatterbox()
    // Expect not to find Send button before typing
    expect(sendBtn()).not.toBeInTheDocument()
    // Now type a message
    type(fetchChatBox(), 'Shhh is a sound after all')
    // It should be there
    expect(sendBtn()).toBeInTheDocument()
  })
})
