export const px2vw = (size, width = 1440) => `${(size / width) * 100}vw`
export const px2vh = (size, height = 900) => `${(size / height) * 100}vh`

